package com.ingenotech.charts.adsl;

import java.util.Date;

/**
 * Single set of DSL measurements.
 */
public interface LineMeasurement {

    Date getTimestamp();
    
    Object getValue(MeasurementName name);
}
