/*
 * Measure DSL Performance using the router.
 * Copyright (c) 2014 Ed Randall
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ingenotech.charts.adsl;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.InetAddress;

import com.ingenotech.charts.adsl.tplink.ConnectionInfo;
import com.ingenotech.charts.adsl.tplink.TPLinkTelnetConnection;

/**
 * Measure DSL performance.
 * Currently works for TP-Link TD-W8970 only.
 * Uses the telnet interface.
 * TODO: Pass measurements to chart
 * @see http://jchart2d.sourceforge.net/usage.shtml
 * 
 * @author Ed
 */
public class DSLPerformance implements Runnable {

    /**
     * Main program.
     *
     * @param args
     *        args[0] - host name / address of the router
     *        args[1] - router admin username
     *        args[2] - router admin password
     * @throws Exception if anything fails.
     */
    public static void main(String[] args) throws Exception {

        try {
            new DSLPerformance(args);
            
        } finally {
        }
    }

	private static final int DEFAULT_POLLINTERVAL = 60;
    
    private DynamicChart chart;
    private TPLinkTelnetConnection connection;
    private Thread connectionThread;
    private volatile boolean exit = false;
    private int pollInterval;
    
    public DSLPerformance(String[] args) throws Exception {
        ConnectionInfo ci = new ConnectionInfo(InetAddress.getByName(args[0]), args[1], args[2]);
        this.connection = new TPLinkTelnetConnection(ci);
		
		try {
			pollInterval = Integer.parseInt(args[3]);
		} catch (Exception ex) {
			pollInterval = DEFAULT_POLLINTERVAL;
		}
		
        
        WindowListener windowListener = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                exit();
            }
        };
        this.chart = new DynamicChart(windowListener);
        
        connectionThread = new Thread(this);
        connectionThread.start();
    }
    
    
    private void exit() {
        exit = true;
        chart.close();
        connectionThread.interrupt();
    }
    
    public void run() {
		final long pi = pollInterval * 1000;
		
        while (!exit) {
            try {
                System.err.println("connecting...");
                this.connection.connect();

                while (!exit) {
                    LineMeasurement stat = connection.read();
                    System.out.println(stat);
                    chart.addMeasurement( stat );
                    Thread.sleep(pi);
                }

            } catch (IOException ex) {
                System.err.println("measurement thread error:"+ex);

            } catch (InterruptedException ix) {
                System.err.println("measurement thread interrupted");
                
            } finally {
                this.connection.close();
            }
        }
        System.err.println("exited.");
        System.exit(0);
    }

    
}
