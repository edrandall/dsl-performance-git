package com.ingenotech.charts.adsl.tplink;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.ingenotech.charts.adsl.MeasurementName;
import com.ingenotech.charts.adsl.LineMeasurement;

/**
 * Parse values from a single TPLink line measurement.
 * @author Ed
 */
public class TPLinkLineMeasuement implements LineMeasurement {

    private final Date timestamp;
    
    private final Map<MeasurementName,Object> statistics = new HashMap<MeasurementName,Object>();

    public TPLinkLineMeasuement(String statisticsText) {
        timestamp = new Date(); 
        String[] lines = StringUtils.split(statisticsText, "\r\n");
        int l = -1;
        while (++l < lines.length){
            if (lines[l].equals("{")) {
                break;
            }
        }
        while (++l < lines.length) {
            if (lines[l].equals("}")) {
                break;
            }
            String[] statistic = StringUtils.split(lines[l], "=");
            if (statistic.length == 2) {
                TPLinkNames name = TPLinkNames.forName(statistic[0]);
                if (name != null) {
                    Object value = name.parse( statistic[1] ); 
                    statistics.put(name.getMeasurementName(), value);
                }
            }
        }
    }


    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public Object getValue(MeasurementName name) {
        return statistics.get(name);
    }

    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{date:"+timestamp);
        sb.append("; UP: Rate:"+getValue(MeasurementName.UpstreamRate));
        sb.append("; Max:"+getValue(MeasurementName.UpstreamMaxRate));
        sb.append("; SNMargin:"+getValue(MeasurementName.UpstreamNoiseMargin));
        sb.append("; Atten:"+getValue(MeasurementName.UpstreamAttenuation));
        sb.append("; Power:"+getValue(MeasurementName.UpstreamPower));
        sb.append("; DOWN: Rate:"+getValue(MeasurementName.DownstreamRate));
        sb.append("; Max:"+getValue(MeasurementName.DownstreamMaxRate));
        sb.append("; SNMargin:"+getValue(MeasurementName.DownstreamNoiseMargin));
        sb.append("; Atten:"+getValue(MeasurementName.DownstreamAttenuation));
        sb.append("; Power:"+getValue(MeasurementName.DownstreamPower));
        sb.append("}");
        return sb.toString();
    }
    
    
    /**
     * Convert from a String to the right kind of Object for the statistic.
     */
    private static interface ValueConverter {
        Object convert(String value);
    }
    
    
    /**
     * Converters that are needed for TPLinkNames.
     */
    private static class TPLinkConverters {
        
        private static final ValueConverter RATE_CONVERTER = new ValueConverter() {
            public Object convert(String value) {
                return new BigDecimal(value);
            }
        };
                
        private static final ValueConverter DB_CONVERTER = new ValueConverter() {
            public Object convert(String value) {
                BigDecimal db10 = new BigDecimal(value);
                return db10.divide(BigDecimal.TEN);
            }
        };
    }

    
    /**
     * Map TPLink statistic names to our MeasurementName enum.
     */
    private static enum TPLinkNames {
        UPSTREAMCURRRATE        (MeasurementName.UpstreamRate, TPLinkConverters.RATE_CONVERTER),
        UPSTREAMMAXRATE         (MeasurementName.UpstreamMaxRate, TPLinkConverters.RATE_CONVERTER),
        UPSTREAMNOISEMARGIN     (MeasurementName.UpstreamNoiseMargin, TPLinkConverters.DB_CONVERTER),
        UPSTREAMATTENUATION     (MeasurementName.UpstreamAttenuation, TPLinkConverters.DB_CONVERTER),
        UPSTREAMPOWER           (MeasurementName.UpstreamPower, TPLinkConverters.DB_CONVERTER),
        DOWNSTREAMCURRRATE      (MeasurementName.DownstreamRate, TPLinkConverters.RATE_CONVERTER),
        DOWNSTREAMMAXRATE       (MeasurementName.DownstreamMaxRate, TPLinkConverters.RATE_CONVERTER),
        DOWNSTREAMNOISEMARGIN   (MeasurementName.DownstreamNoiseMargin, TPLinkConverters.DB_CONVERTER),
        DOWNSTREAMATTENUATION   (MeasurementName.DownstreamAttenuation, TPLinkConverters.DB_CONVERTER),
        DOWNSTREAMPOWER         (MeasurementName.DownstreamPower, TPLinkConverters.DB_CONVERTER);

        private final MeasurementName statisticName;
        
        private final ValueConverter converter;

        private TPLinkNames(MeasurementName statisticName,
                            ValueConverter converter) {
            this.statisticName = statisticName;
            this.converter = converter;
        }
        
        public Object parse(String value) {
            return converter.convert(value);
        }

        public MeasurementName getMeasurementName() {
            return statisticName;
        }
        
        public static TPLinkNames forName(String text) {
            try {
                return TPLinkNames.valueOf(text.toUpperCase());
            } catch (IllegalArgumentException ex) {
            }
            return null;
        }
    }
}
