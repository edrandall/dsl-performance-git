package com.ingenotech.charts.adsl.tplink;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.apache.commons.net.telnet.TelnetClient;

import com.ingenotech.charts.adsl.LineMeasurement;

/**
 * Connect to the TP-Link router and read line measurements.
 *
 * @author Ed
 */
public class TPLinkTelnetConnection {

    private ConnectionInfo connectionInfo;
    
    private TelnetClient telnet;
    private InputStream input;
    private PrintWriter writer;

    // Timeout 5s
    private static final int CONNECT_TIMEOUT = 5000;

    
    public TPLinkTelnetConnection(ConnectionInfo info) {
        this.connectionInfo = info;
        this.telnet = new TelnetClient();
        this.telnet.setDefaultTimeout(10000);
    }

    
    /**
     * Read a single line measurement.
     * @throws IOException
     */
    public LineMeasurement read() throws IOException {
        writer.println("adsl show info");
        readUntil("adsl show info");
        String statisticsText = readUntil("TP-LINK(conf)#");
        return new TPLinkLineMeasuement(statisticsText);
    }
    
    
    /**
     * Close the telnet connection
     */
    public void close() {
        if (telnet != null && telnet.isConnected()) {
            try {
                writer = null;
                input = null;
                telnet.disconnect();
            } catch (IOException ex) {
            }
        }
    }
    
    
    /**
     * Log in to the router.
     * @throws IOException
     */
    public void connect() throws IOException {

        close();

        // Connect to the specified server
        telnet.setConnectTimeout(CONNECT_TIMEOUT);
        telnet.setReaderThread(true);
        telnet.connect(connectionInfo.getHost(), connectionInfo.getPort());

        // Get input and output stream references
        //input = new BufferedReader(new InputStreamReader(telnet.getInputStream()));
        writer = new PrintWriter(telnet.getOutputStream(), true);
        input = telnet.getInputStream();
        
        readUntil("username:");
        writer.println(connectionInfo.getUsername());
        
        readUntil("password:");
        writer.println(connectionInfo.getPassword());
        writer.println();
        readUntil("TP-LINK(conf)#");
        System.out.println("Logged in OK.");

    }
    
    
    private String readUntil(String pattern) throws IOException {  
        SimpleMatcher matcher = new SimpleMatcher(pattern);  
        StringBuilder sb = new StringBuilder();  

        boolean patternFound = false;
        final long timeoutTime = System.currentTimeMillis() + CONNECT_TIMEOUT;
        while (!patternFound) {
            while (input.available() > 0) {
                char ch = (char)input.read();
                if (Character.isISOControl(ch) && !Character.isWhitespace(ch))
                    continue;
                sb.append( ch );
                if (matcher.matchEnd(sb)) {
                    patternFound = true;
                    break;
                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
            }
            if (System.currentTimeMillis() > timeoutTime) {
                throw new TimeoutException("Expecting:>"+pattern +"< but got:>"+sb+"<");
            }
        }

        //System.err.println(sb);
        return sb.toString();
    }  
    
    
    /**
     * Efficiently(?) check for match against the trailing end of a StringBuilder.
     */
    private static class SimpleMatcher {
        
        private char[] match;
        
        SimpleMatcher(String pattern) {
            match = pattern.toLowerCase().toCharArray();
        }
        
        boolean matchEnd(StringBuilder search) {
            int iMatch = match.length;
            int iSearch = search.length();
            if (iSearch < iMatch) {
                return false;
            }
            while (iMatch > 0) {
                --iMatch;
                --iSearch;
                if (match[iMatch] != Character.toLowerCase( search.charAt(iSearch) )) {
                    return false;
                }
            }
            return true;
        }
    }
}
