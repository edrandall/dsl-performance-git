
package com.ingenotech.charts.adsl;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis;
import info.monitorenter.gui.chart.IAxisScalePolicy;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.axis.AAxis;
import info.monitorenter.gui.chart.axis.AxisLinear;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.gui.chart.labelformatters.LabelFormatterDate;
import java.awt.Color;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;

/**
 * Basic chart, copied from Chart2D MinimalDynamicChart example.
 * @see http://jchart2d.sourceforge.net/usage.shtml
 * @author Achim Westermann
 * @author Ed
 */
public class DynamicChart {
  
    private final JFrame frame;
    
    private final ITrace2D trace1;
    private final ITrace2D trace2;
//  private final ITrace2D trace3;
    
   
    
    public DynamicChart(WindowListener windowListener) {
        // Create a chart:  
        Chart2D chart = new Chart2D();

        IAxis<?> timeAxis = chart.getAxisX();
        timeAxis.setFormatter( new LabelFormatterDate( new SimpleDateFormat("HH:mm:ss"))) ;
        
        IAxis<?> snrAxis = chart.getAxisY();
        snrAxis.getAxisTitle().setTitle(MeasurementName.DownstreamNoiseMargin.getUnit());
        
        AAxis<IAxisScalePolicy> rateAxis = new AxisLinear<IAxisScalePolicy>();
        rateAxis.getAxisTitle().setTitle(MeasurementName.DownstreamMaxRate.getUnit());
        chart.addAxisYRight(rateAxis);
        
//      AAxis<IAxisScalePolicy> attenAxis = new AxisLinear<IAxisScalePolicy>();
//      attenAxis.getAxisTitle().setTitle(MeasurementName.DownstreamAttenuation.getUnit());
//      chart.addAxisYLeft(attenAxis);

        
        // Create an ITrace: 
        // Note that dynamic charts need limited amount of values!!! 
        trace1 = new Trace2DLtd(200, MeasurementName.DownstreamNoiseMargin.getDescription()); 
        trace1.setColor(Color.RED);
        chart.addTrace(trace1, timeAxis, snrAxis);

        trace2 = new Trace2DLtd(200, MeasurementName.DownstreamMaxRate.getDescription()); 
        trace2.setColor(Color.BLUE);
        chart.addTrace(trace2, timeAxis, rateAxis);

//      trace3 = new Trace2DLtd(200, MeasurementName.DownstreamAttenuation.getDescription()); 
//      trace3.setColor(Color.RED);
//      chart.addTrace(trace3, timeAxis, attenAxis);

        // Create a frame containing the chart. 
        frame = new JFrame("DSL Performance");
        frame.getContentPane().add(chart);
        frame.setSize(400,300);

        // Enable the termination button [cross on the upper right edge]: 
        frame.addWindowListener(windowListener);
        frame.setVisible(true); 
    }
  
    
    public void addMeasurement(LineMeasurement data) {
        long time = data.getTimestamp().getTime();
        trace1.addPoint( time, ((Number)data.getValue(MeasurementName.DownstreamNoiseMargin)).doubleValue() );
        trace2.addPoint( time, ((Number)data.getValue(MeasurementName.DownstreamMaxRate)).doubleValue() );
//      trace3.addPoint( time, ((Number)data.getValue(MeasurementName.DownstreamAttenuation)).doubleValue() );
    }
    
    
    public void close() {
        frame.setVisible(false);
        frame.dispose();
    }
}
      