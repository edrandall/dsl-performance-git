package com.ingenotech.charts.adsl;

public enum MeasurementName {

    UpstreamRate ("kbps", "Uplink data rate"),
    UpstreamMaxRate ("kbps", "Uplink sync rate"),
    UpstreamNoiseMargin ("dB", "Uplink noise margin"),
    UpstreamAttenuation ("dB", "Tx line attenuation"),
    UpstreamPower ("dB", "Tx power"),
    DownstreamRate ("kbps", "Downlink data rate"),
    DownstreamMaxRate ("kbps", "Downlink sync rate"),
    DownstreamNoiseMargin ("dB", "Downlink noise margin"),
    DownstreamAttenuation ("dB", "Rx line attenuation"),
    DownstreamPower ("dB", "Rx power");
    
    private final String unit;
    private final String description;
    
    private MeasurementName(String unit, String description) {
        this.unit = unit;
        this.description = description;
    }
    
    public String getUnit() {
        return unit;
    }
    
    public String getDescription() {
        return description;
    }
}
