package com.ingenotech.charts.adsl.tplink;

import java.net.InetAddress;

public class ConnectionInfo {

    private static final int DEFAULT_PORT = 23;

    private final InetAddress host;
    private final int port;
    private final String username;
    private final String password;
    
    public ConnectionInfo(InetAddress host,
                          String username,
                          String password) {
        this(host, DEFAULT_PORT, username, password);
    }
    
    public ConnectionInfo(InetAddress host,
                          int port,
                          String username,
                          String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    public InetAddress getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
