package com.ingenotech.charts.adsl.tplink;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;

public class TPLinkHTTPConnection {

	public static void main(String[] args) throws Exception {
		URI loginUrl = new URI(args[0]);
		String user = args[1];
		String pass = args[2];
		URI statusURI = new URI(args[3]);

		new TPLinkHTTPConnection(loginUrl, user, pass, statusURI);
	}
	
	public TPLinkHTTPConnection(URI loginURI,
					   String username,
					   String password,
					   URI statusURI) throws ClientProtocolException, IOException {

		// TP-Link WD-8970
//		var password = $("pcPassword").value;
//		var userName = $("userName").value;
//		auth = "Basic "+Base64Encoding(userName+":"+password);
//		document.cookie = "Authorization=" + auth;
//		window.location.reload();
		
		byte[] authData = (username+":"+password).getBytes(Charset.forName("UTF8"));
		String authString = "Basic "+Base64.encodeBase64String(authData);
		System.out.println(authString);
		BasicClientCookie cookie = new BasicClientCookie("Authorization", authString);
		cookie.setVersion(0);
		cookie.setDomain(loginURI.getHost());
		cookie.setPath("/");
		CookieStore cookieStore = new BasicCookieStore();
		cookieStore.addCookie(cookie);
		
		RequestConfig globalConfig = RequestConfig.custom()
		        								  .setCookieSpec(CookieSpecs.BEST_MATCH)
		        								  .build();
		CloseableHttpClient httpclient = HttpClients.custom()
		        								    .setDefaultRequestConfig(globalConfig)
		        								    .setDefaultCookieStore(cookieStore)
		        								    .build();
		HttpGet httpGet;
		CloseableHttpResponse response;
		
		httpGet = new HttpGet(loginURI);
		response = httpclient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String content = EntityUtils.toString(entity);
			System.out.println(content);
		} finally {
			response.close();
		}
		
		httpGet = new HttpGet(statusURI);
		response = httpclient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String content = EntityUtils.toString(entity);
			System.out.println(content);
		} finally {
			response.close();
		}
	}
}
